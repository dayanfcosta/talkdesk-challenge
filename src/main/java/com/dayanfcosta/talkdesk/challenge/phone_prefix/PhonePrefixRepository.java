package com.dayanfcosta.talkdesk.challenge.phone_prefix;

import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;
import org.apache.commons.collections4.trie.PatriciaTrie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileCopyUtils;

@Repository
public class PhonePrefixRepository implements InitializingBean {

  private static final Logger LOGGER = LoggerFactory.getLogger(PhonePrefixRepository.class);

  private final PatriciaTrie<String> prefixes;
  private final ResourceLoader resourceLoader;

  PhonePrefixRepository(final ResourceLoader resourceLoader) {
    this.resourceLoader = resourceLoader;
    prefixes = new PatriciaTrie<>();
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    LOGGER.debug("Initializing database...");
    final var resource = resourceLoader.getResource("classpath:prefixes.txt");
    try (final var inputStream = resource.getInputStream()) {
      final var bytes = FileCopyUtils.copyToByteArray(inputStream);
      final var rawData = new String(bytes, StandardCharsets.UTF_8);
      final var prefixNumbers = rawData.split("\n");
      Stream.of(prefixNumbers).forEach(prefix -> prefixes.put(prefix, prefix));
      LOGGER.info("Database initialized. Total of elements: {}", prefixes.size());
    } catch (final Exception ex) {
      LOGGER.error("Unable to load repository: ", ex);
      throw ex;
    }
  }

  /**
   * Given a phoneNumber, returns its prefix.
   *
   * @param phoneNumber
   * @return Prefix of a phone number
   */
  public String find(final String phoneNumber) {
    final String prefixo = prefixes.selectValue(phoneNumber);
    return !prefixo.equals(phoneNumber) ? prefixo : "";
  }
}
