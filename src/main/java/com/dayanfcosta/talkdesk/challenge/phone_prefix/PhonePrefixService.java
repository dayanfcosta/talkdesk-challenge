package com.dayanfcosta.talkdesk.challenge.phone_prefix;

import com.dayanfcosta.talkdesk.challenge.sector.BusinessSector;
import org.springframework.stereotype.Service;

@Service
public class PhonePrefixService {

  private final PhonePrefixRepository repository;

  public PhonePrefixService(final PhonePrefixRepository repository) {
    this.repository = repository;
  }

  public PhonePrefix findPrefix(final BusinessSector businessSector) {
    final var prefix = repository.find(normalizeNumber(businessSector.getNumber()));
    return new PhonePrefix(prefix, businessSector.getSector());
  }

  private String normalizeNumber(final String phoneNumber) {
    return phoneNumber
        .replace("+", "")
        .replace(" ", "");
  }

}
