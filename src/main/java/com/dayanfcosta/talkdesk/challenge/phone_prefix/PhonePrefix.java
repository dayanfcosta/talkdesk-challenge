package com.dayanfcosta.talkdesk.challenge.phone_prefix;

import java.util.Objects;

public class PhonePrefix {

  private final String prefix;
  private final String sector;

  public PhonePrefix(final String prefix, final String sector) {
    this.prefix = prefix;
    this.sector = sector;
  }

  public String getPrefix() {
    return prefix;
  }

  public String getSector() {
    return sector;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final PhonePrefix that = (PhonePrefix) o;
    return prefix.equals(that.prefix) && sector.equals(that.sector);
  }

  @Override
  public int hashCode() {
    return Objects.hash(prefix, sector);
  }
}
