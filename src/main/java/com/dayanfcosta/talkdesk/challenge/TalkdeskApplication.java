package com.dayanfcosta.talkdesk.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TalkdeskApplication {

  public static void main(final String[] args) {
    SpringApplication.run(TalkdeskApplication.class, args);
  }

}
