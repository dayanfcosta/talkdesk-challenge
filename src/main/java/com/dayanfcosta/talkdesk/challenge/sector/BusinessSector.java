package com.dayanfcosta.talkdesk.challenge.sector;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Objects;
import java.util.StringJoiner;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessSector {

  private String number;
  private String sector;
  private boolean valid;

  BusinessSector() {
  }

  public BusinessSector(final String number, final String sector, final boolean valid) {
    this.number = number;
    this.sector = sector;
    this.valid = valid;
  }

  public String getNumber() {
    return number;
  }

  public String getSector() {
    return sector;
  }

  @JsonIgnore
  public boolean isValid() {
    return valid;
  }

  public static BusinessSector validNumber(final BusinessSector sector) {
    return new BusinessSector(sector.getNumber(), sector.getSector(), true);
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final BusinessSector that = (BusinessSector) o;
    return valid == that.valid && number.equals(that.number) && sector.equals(that.sector);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, sector, valid);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", BusinessSector.class.getSimpleName() + "[", "]")
        .add("number='" + number + "'")
        .add("sector='" + sector + "'")
        .add("valid=" + valid)
        .toString();
  }
}
