package com.dayanfcosta.talkdesk.challenge.sector;

import static java.util.stream.Collectors.toList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class BusinessSectorService {

  private static final Logger LOGGER = LoggerFactory.getLogger(BusinessSectorService.class);
  private static final String BASE_URL = "https://challenge-business-sector-api.meza.talkdeskstg.com/sector";

  private final RestTemplate restTemplate;

  public BusinessSectorService(final RestTemplateBuilder restTemplateBuilder) {
    restTemplate = restTemplateBuilder.build();
  }

  public List<BusinessSector> find(final List<String> phoneNumbers) {
    return phoneNumbers.stream()
        .map(this::find)
        .filter(BusinessSector::isValid)
        .collect(toList());
  }

  private BusinessSector find(final String phoneNumber) {
    try {
      LOGGER.debug("Requesting sector for number: {}", phoneNumber);
      final var url = String.format("%s/%s", BASE_URL, phoneNumber);
      final var businessSector = restTemplate.getForEntity(url, BusinessSector.class);
      LOGGER.info("Business sector retrieved: {}", businessSector.getBody());
      Objects.requireNonNull(businessSector.getBody(), "Invalid response body");
      return BusinessSector.validNumber(businessSector.getBody());
    } catch (final HttpClientErrorException ex) {
      LOGGER.warn("Invalid number requested: {}", phoneNumber);
      return getBusinessSector(ex);
    }
  }

  private BusinessSector getBusinessSector(final HttpClientErrorException ex) {
    try {
      final var objectMapper = new ObjectMapper();
      return objectMapper.readValue(ex.getResponseBodyAsString(), BusinessSector.class);
    } catch (final JsonProcessingException exception) {
      LOGGER.error("Error while reading json: {}", ex.getResponseBodyAsString(), exception);
      throw ex;
    }
  }

}
