package com.dayanfcosta.talkdesk.challenge.aggregator;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import com.dayanfcosta.talkdesk.challenge.phone_prefix.PhonePrefix;
import com.dayanfcosta.talkdesk.challenge.phone_prefix.PhonePrefixService;
import com.dayanfcosta.talkdesk.challenge.sector.BusinessSector;
import com.dayanfcosta.talkdesk.challenge.sector.BusinessSectorService;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AggregatorService {

  private static final Logger LOGGER = LoggerFactory.getLogger(AggregatorService.class);

  private final BusinessSectorService businessSectorService;
  private final PhonePrefixService phonePrefixService;

  public AggregatorService(final BusinessSectorService businessSectorService, final PhonePrefixService phonePrefixService) {
    this.businessSectorService = businessSectorService;
    this.phonePrefixService = phonePrefixService;
  }

  public Map<String, Map<String, Long>> aggregate(final List<String> phoneNumbers) {
    LOGGER.debug("Starting the aggregation for numbers: {}", phoneNumbers);
    final var businessSectors = businessSectorService.find(phoneNumbers);
    return phonePrefixesWithSector(businessSectors);
  }

  /**
   * This method is responsible for grouping the sectors of each phone prefix found
   */
  private Map<String, Map<String, Long>> phonePrefixesWithSector(final List<BusinessSector> businessSectors) {
    return businessSectors.stream()
        .map(phonePrefixService::findPrefix)
        .collect(toList()).stream()
        .collect(groupingBy(PhonePrefix::getPrefix, LinkedHashMap::new, groupingBy(PhonePrefix::getSector, counting())));
  }

}
