package com.dayanfcosta.talkdesk.challenge.aggregator;

import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/aggregate")
public class AggregateApi {

  private final AggregatorService service;

  public AggregateApi(final AggregatorService service) {
    this.service = service;
  }

  @PostMapping
  public Map<String, Map<String, Long>> aggregate(@RequestBody final List<String> phoneNumbers) {
    return service.aggregate(phoneNumbers);
  }

}
