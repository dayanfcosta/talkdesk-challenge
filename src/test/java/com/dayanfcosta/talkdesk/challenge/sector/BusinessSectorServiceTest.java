package com.dayanfcosta.talkdesk.challenge.sector;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;

@ExtendWith(SpringExtension.class)
@RestClientTest(BusinessSectorService.class)
class BusinessSectorServiceTest {

  private static final String BASE_URL = "https://challenge-business-sector-api.meza.talkdeskstg.com/sector/";

  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
  private MockRestServiceServer server;
  @Autowired
  private BusinessSectorService service;

  @Test
  void whenFindAnInvalidNumber_shouldReturnAnInvalidatedObject() throws Exception {
    final String sector = objectMapper.writeValueAsString(new BusinessSector("1", "test", false));
    server.expect(requestTo(BASE_URL + "1")).andRespond(withBadRequest().body(sector).contentType(MediaType.APPLICATION_JSON));

    final var businessSector = service.find(singletonList("1"));

    assertThat(businessSector).isNotNull();
    assertThat(businessSector).isEmpty();
  }

  @Test
  void whenFindAValidNumber_shouldReturnAValidatedObject() throws Exception {
    final String sector = objectMapper.writeValueAsString(new BusinessSector("1", "test", false));
    server.expect(requestTo(BASE_URL + "1")).andRespond(withSuccess(sector, MediaType.APPLICATION_JSON));

    final var businessSector = service.find(singletonList("1"));

    assertThat(businessSector).isNotNull();
    assertThat(businessSector).isNotEmpty();
    assertFalse(businessSector.stream().noneMatch(BusinessSector::isValid));
  }

  @Test
  void whenFindAValidNumber_returnsNullBody() {
    server.expect(requestTo(BASE_URL + "1")).andRespond(withSuccess("", MediaType.APPLICATION_JSON));

    assertThatNullPointerException()
        .isThrownBy(() -> service.find(singletonList("1")))
        .withMessage("Invalid response body");
  }
}