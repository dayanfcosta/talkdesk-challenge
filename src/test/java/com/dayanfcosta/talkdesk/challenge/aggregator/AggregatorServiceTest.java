package com.dayanfcosta.talkdesk.challenge.aggregator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import com.dayanfcosta.talkdesk.challenge.phone_prefix.PhonePrefix;
import com.dayanfcosta.talkdesk.challenge.phone_prefix.PhonePrefixService;
import com.dayanfcosta.talkdesk.challenge.sector.BusinessSector;
import com.dayanfcosta.talkdesk.challenge.sector.BusinessSectorService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class AggregatorServiceTest {

  private static final String INVALID_NUMBER = "+1";

  private static final String VALID_API_NUMBER_TECH_1 = "+1983248";
  private static final String VALID_API_NUMBER_TECH_2 = "+1382355";
  private static final String VALID_API_NUMBER_CLOTHING = "+147 8192";
  private static final String VALID_API_NUMBER_BANKING = "+4439877";

  private static final String TECH_NUMBER_1_NORMALIZED = "1983248";
  private static final String TECH_NUMBER_2_NORMALIZED = "1382355";
  private static final String CLOTHING_NUMBER_NORMALIZED = "1478192";
  private static final String BANKING_NUMBER_NORMALIZED = "4439877";

  @Mock
  private BusinessSectorService businessSectorService;
  @Mock
  private PhonePrefixService phonePrefixService;

  @InjectMocks
  private AggregatorService service;


  @BeforeEach
  void setUp() {
    initMocks(this);

    when(phonePrefixService.findPrefix(sectorOf(BANKING_NUMBER_NORMALIZED, "Banking", true)))
        .thenReturn(new PhonePrefix("1", "Technology"));
    when(phonePrefixService.findPrefix(sectorOf(TECH_NUMBER_1_NORMALIZED, "Technology", true)))
        .thenReturn(new PhonePrefix("1", "Technology"));
    when(phonePrefixService.findPrefix(sectorOf(TECH_NUMBER_2_NORMALIZED, "Technology", true)))
        .thenReturn(new PhonePrefix("1", "Clothing"));
    when(phonePrefixService.findPrefix(sectorOf(CLOTHING_NUMBER_NORMALIZED, "Clothing", true)))
        .thenReturn(new PhonePrefix("44", "Banking"));
  }

  @Test
  void whenAggregatingValues_returnAllValues() {
    final var validNumbers = List.of(VALID_API_NUMBER_TECH_1, VALID_API_NUMBER_TECH_2, VALID_API_NUMBER_BANKING, VALID_API_NUMBER_CLOTHING);
    when(businessSectorService.find(validNumbers)).thenReturn(onlyValid());

    final var aggregate = service.aggregate(validNumbers);

    assertThat(aggregate).hasSize(2);
    verify(businessSectorService, times(1)).find(any());
    verify(phonePrefixService, times(validNumbers.size())).findPrefix(any());
  }

  @Test
  void whenAggregatingWithInvalidNumbers_returnJustValidOnes() {
    final var withInvalidNumber = List.of(VALID_API_NUMBER_TECH_1, VALID_API_NUMBER_TECH_2, VALID_API_NUMBER_BANKING, INVALID_NUMBER);
    when(businessSectorService.find(withInvalidNumber)).thenReturn(withInvalid());

    final var aggregate = service.aggregate(withInvalidNumber);

    assertThat(aggregate).hasSize(1);
    assertThat(aggregate.get("1").values()).hasSize(2);
    verify(businessSectorService, times(1)).find(any());
    verify(phonePrefixService, times(withInvalidNumber.size() - 1)).findPrefix(any());
  }

  private List<BusinessSector> onlyValid() {
    return List.of(
        sectorOf(BANKING_NUMBER_NORMALIZED, "Banking", true),
        sectorOf(TECH_NUMBER_1_NORMALIZED, "Technology", true),
        sectorOf(TECH_NUMBER_2_NORMALIZED, "Technology", true),
        sectorOf(CLOTHING_NUMBER_NORMALIZED, "Clothing", true)
    );
  }

  private List<BusinessSector> withInvalid() {
    return List.of(
        sectorOf(BANKING_NUMBER_NORMALIZED, "Banking", true),
        sectorOf(TECH_NUMBER_1_NORMALIZED, "Technology", true),
        sectorOf(TECH_NUMBER_2_NORMALIZED, "Technology", true)
    );
  }

  private BusinessSector sectorOf(final String number, final String sector, final boolean valid) {
    return new BusinessSector(number, sector, valid);
  }

}