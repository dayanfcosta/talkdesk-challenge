package com.dayanfcosta.talkdesk.challenge.phone_prefix;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class PhonePrefixRepositoryTest {

  @Autowired
  private ResourceLoader resourceLoader;

  private PhonePrefixRepository repository;

  @BeforeEach
  void setUp() throws Exception {
    repository = new PhonePrefixRepository(resourceLoader);
    repository.afterPropertiesSet();
  }

  @Test
  void whenFindingPrefix_returnRightPrefix() {
    var prefix = repository.find("1983236248");
    assertThat(prefix).isEqualTo("1");

    prefix = repository.find("4439877");
    assertThat(prefix).isEqualTo("44");
  }

}