# Talkdesk Challenge

Backend coding challenge proposed by Talkdesk during interview phase

## Technology stack
* Java 11
* Spring boot
* Apache commons collections 4
* Mockito for mocking tests 
* JUnit 5
* Docker and Docker compose

## Getting started  
To be able to run this project you must have **docker and docker compose** installed.  
Their installation guide are in:  https://docs.docker.com/install/ and https://docs.docker.com/compose/install/, please visit their websites.

## Running this project  
**YOU MUST HAVE DOCKER AND DOCKER COMPOSE INSTALLED**
 * Go to the project root folder and run the following command in your terminal:
    * `docker-compose up web`
    
## Project Organization
- The api source code is under the `src` folder;
- In the folder `artifacts` we have the `api.jar` that is our deployable artifact;

## Endpoint
POST http://localhost:8080/aggregate with a string array in the body 